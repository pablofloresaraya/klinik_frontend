import React, {useState, useEffect} from 'react';

export function TabApplicationFunction(props){
    const styles_tabopen = {
        minWidth: '3000px',
    };
  
    const [scrollerRightDisplay,setScrollerRightDisplay] = useState({display: props.scrollerRightDisplay});
    const [scrollerLeftDisplay,setScrollerLeftDisplay] = useState({display: props.setScrollerLeftDisplay});
    
    useEffect(
      () => {
        console.log('TabApplicationFunction: useEffect');
      }
    );

    return(
    <div className="w-100">
        <div id="scroller-left" className="scroller scroller-left float-left" style={scrollerLeftDisplay}><i className="fa fa-chevron-left"></i></div>
        <div id="scroller-right" className="scroller scroller-right float-right" style={scrollerRightDisplay}><i className="fa fa-chevron-right"></i></div>
        <div id="wrapper" className="wrapper">
            <ul className="nav nav-tabs list ml-1 mt-1" id="myTabAppOpen" role="tablist"style={styles_tabopen}>
                <li className="nav-item">
                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inicio</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Aplicacion 1</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 2</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 3</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 4</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 5</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 6</a>
                </li>
            </ul>
        </div>
    </div>
    );

}