import React, {Component} from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from './components/_helpers';
import { alertActions } from './components/_actions';
import { PrivateRoute } from './components/_components';

import {Home} from './components/home/home'
import {Login} from './components/login/login'
//import logo from './logo.svg';
import './App.css';
import './css/tabscroll.css';

class App extends Component{
  constructor(props) {
      super(props);

      history.listen((location, action) => {
          // clear alert on location change
          this.props.clearAlerts();
      });
  }
  
  render(){
    return (
      <div>
        {alert.message &&
            <div className={`alert ${alert.type}`}>{alert.message}</div>
        }
        <Router history={history}>
            <Switch>
                <PrivateRoute exact path="/" component={Home} />
                <Route path="/login" component={Login} />
                <Redirect from="*" to="/" />
            </Switch>
        </Router>
      </div>
    );
  }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export default connectedApp;
