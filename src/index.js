import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import './index.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import './css/styles.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import '@fortawesome/fontawesome-free/js/all.js';
import './css/menu-form.css';
//import './navTabScroll.bootstrap4.js';

import { store } from './components/_helpers';
import App from './App';

import * as serviceWorker from './serviceWorker';

render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
