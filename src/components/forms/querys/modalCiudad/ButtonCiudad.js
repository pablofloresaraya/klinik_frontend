import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';

export function ButtonCiudad(props){

    const cities = props.data;
    
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    const columns = [
        {
            dataField: 'code',
            text: 'Código',
            headerAlign: 'center',
            headerStyle: () => {
                return { width: "15%" };
            }
        }, {
            dataField: 'name',
            text: 'Nombre',
            headerAlign: 'center'           
        }
    ];

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
            props.ciudad(row.id,row.name);
            setShow(false);
        }
    };

    return(
        <>
            <button className="btn btn-outline-info btn-sm" type="button" onClick={handleShow} disabled={!props.data.length}>
                <i className="fas fa-list"></i>
            </button>
            <Modal show={show} onHide={handleClose} size="lg" maria-labelledby="ModalHeader">
                <Modal.Header closeButton>
                    <Modal.Title id='ModalHeader'>Seleccione Ciudad</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <BootstrapTable 
                        bootstrap4 
                        keyField='id' 
                        data={cities} 
                        columns={columns} 
                        rowEvents={rowEvents}
                        rowStyle={{cursor:'pointer'}}
                    /> 
                </Modal.Body>
                <Modal.Footer>                   
                    <button className='btn btn-primary' onClick={handleClose}>
                        Cerrar
                    </button>
                </Modal.Footer>
            </Modal> 
        </>
    );
    
}

export default ButtonCiudad;