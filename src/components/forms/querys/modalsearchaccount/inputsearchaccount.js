import $ from 'jquery';

import { config } from '../../../_constants';
import { authHeader } from '../../../_helpers';

export function inputSearchAccount(p_code,p_origen,p_destino) { 

    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/accounts?var1=1&code=${p_code}`, requestOptions)
        .then(results => {
            return results.json();
        })
        .then(results => {
            let data = results.data;
            if(data!==undefined){                
                return data;
            }
        })
    
}