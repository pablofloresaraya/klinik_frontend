import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
//import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
//import filterFactory, { textFilter, Comparator } from 'react-bootstrap-table2-filter';

export function ButtonRutProv(props){
    
    const ruts = props.data;
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const { SearchBar, ClearSearchButton } = Search;
    
    const columns = [
        {
            dataField: 'rut',
            text: 'Rut',
            id:'rut',
            headerAlign: 'center',
            headerStyle: () => {
                return { width: "15%" };
            }
        }, {
            dataField: 'name',
            text: 'Nombre',
            headerAlign: 'center',
            //filter: textFilter()           
        }
    ];

    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 10,
        lastPageText: '>>',
        firstPageText: '<<',
        nextPageText: '>',
        prePageText: '<',
        showTotal: false,
        alwaysShowAllBtns: true,
        onPageChange: function (page, sizePerPage) {
          console.log('page', page);
          console.log('sizePerPage', sizePerPage);
        },
        onSizePerPageChange: function (page, sizePerPage) {
          console.log('page', page);
          console.log('sizePerPage', sizePerPage);
        }
    });

    const rowEvents = {
        onClick: (e, row, rowIndex) => {
            props.rut(row.run);
            setShow(false);
        }
    };

    return(
        <>
            <style type="text/css">
                {`
                .btn-secondary{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
        
                .btn-secondary:hover{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
        
                .btn-secondary.dropdown-toggle:focus{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
                .react-bs-table th {
                    vertical-align: center;
                }
                `}
            </style>

            <button className="btn btn-outline-info btn-sm" type="button" onClick={handleShow}>
                <i className="fas fa-list"></i>
            </button>
            <Modal show={show} onHide={handleClose} size="lg" maria-labelledby="ModalHeader">
                <Modal.Header closeButton>
                    <Modal.Title id='ModalHeader'>Seleccione Rut</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ToolkitProvider
                        bootstrap4
                        keyField='id'
                        data={ruts}
                        columns={columns}
                        search
                    >
                        {
                            props => (
                                <div>
                                    <div className="text-right">    
                                        <SearchBar {...props.searchProps} placeholder="Buscar" />                                                               
                                    </div>
                                    <BootstrapTable
                                        rowEvents={rowEvents}
                                        rowStyle={{cursor:'pointer'}}
                                        pagination={pagination}
                                        {...props.baseProps}
                                    />
                                </div>                                 
                            )
                        }
                    </ToolkitProvider>                 
                </Modal.Body>
                <Modal.Footer>                   
                    <button className='btn btn-primary' onClick={handleClose}>
                        Cerrar
                    </button>
                </Modal.Footer>
            </Modal> 
        </>
    );
    
}

export default ButtonRutProv;