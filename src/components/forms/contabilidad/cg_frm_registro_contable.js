import React, {Component} from 'react';

import {ButtonSearchVoucherType} from '../querys/modalsearchvouchertype/buttonsearchvouchertype'
import {ButtonSearchAccount} from '../querys/modalsearchaccount/buttonsearchaccount'
import {MenuForm} from '../menuform/menuform'

export class CgFrmRegistroContable extends Component{

    constructor(props){ 
        super(props);

        this.state = {
            general: {numero: "", fecha: "", tipo_comprobante: "", glosa: "", total_debe: 0, total_haber: 0},
            detalle: [{entry_id: null, line: 1, id: null, code: '', name: '', debit: 0, credit: 0},
                      {entry_id: null, line: 2, id: null, code: '', name: '', debit: 0, credit: 0},],
            vouchertype: {},
            nuevo: false,
        }

        this._setVoucherType = this._setVoucherType.bind(this);
        this._handleChangeAccountDetailAccount = this._handleChangeAccountDetailAccount.bind(this);
        this._handleChangeAccountDetailAccountInput = this._handleChangeAccountDetailAccountInput.bind(this);
        this._handleChangeAccountDetailDebit = this._handleChangeAccountDetailDebit.bind(this);
        this._handleChangeAccountDetailCredit = this._handleChangeAccountDetailCredit.bind(this);
        this._handleOnClickBtnNuevo = this._handleOnClickBtnNuevo.bind(this);
        this._handleOnClickBtnGuardar = this._handleOnClickBtnGuardar.bind(this);
        this._handleOnClickBtnConsultar = this._handleOnClickBtnConsultar.bind(this);
        this._handleOnClickBtnAnular = this._handleOnClickBtnAnular.bind(this);
        this._handleOnClickBtnEliminar = this._handleOnClickBtnEliminar.bind(this);
    }
    
    _setVoucherType(p_data){
        this.setState(
            {
                vouchertype: {id: p_data.id, company_id: p_data.company_id, voucher_type_code: p_data.voucher_type_code
                            , name: p_data.name, description: p_data.description, class_type: p_data.class_type},
            }
        )
        this.setState({general: {tipo_comprobante: p_data.voucher_type_code}});
    }
    
    _handleChangeNumero(event) {
        this.setState({general: {numero: event.target.value}});
    }

    _handleChangeFecha(event) {
        this.setState({general: {fecha: event.target.value}});
    }

    _handleChangeTipoComprobante(event) {
        this.setState({general: {tipo_comprobante: event.target.value}});
    }

    _handleChangeGlosa(event) {
        this.setState({general: {glosa: event.target.value}});
    }

    _handleClickAddRow(event){
        var v_detalle = this.state.detalle;
        var v_line = v_detalle.length;
        
        if(v_line===0) v_line=1
        else v_line++;
        
        v_detalle.push({entry_id: null, line: v_line, id: null, code: '', name: '', debit: 0, credit: 0})
        
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailAccount(p_line, p_id, p_code, p_name) {
        var v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        console.log("p_line",p_line,p_id,p_code,p_name);
        console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailAccountInput(p_line, p_code) {
        /*
        let v_id = null;
        let v_name = null;

        let v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        console.log("p_line",p_line,p_id,p_code,p_name);
        console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
        */
    }

    _handleChangeAccountDetailDebit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["debit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailCredit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["credit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleClickCopyRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.push({entry_id: v_detalle[p_index].entry_id, line: v_detalle[p_index].line, account_id: v_detalle[p_index].account_id, debit: v_detalle[p_index].debit, credit: v_detalle[p_index].credit})
        this.setState({detalle: v_detalle});
    }

    _handleClickDelRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.splice(p_index, 1);
        this.setState({detalle: v_detalle});
    }

    _handleOnClickBtnNuevo(){
        this.setState({nuevo: true});
    }

    _handleOnClickBtnGuardar(){
        //this.setState({nuevo: false});
    }

    _handleOnClickBtnConsultar(){
        console.log("Consultar");
    }

    _handleOnClickBtnAnular(){
        console.log("Anular");
    }

    _handleOnClickBtnEliminar(){
        console.log("Eliminar");
    }
    
    render(){
        const menuitems = [ {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar},
                            {type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar}];
        return(
        <>
            <MenuForm menuitems={menuitems} />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <strong>Informaci&oacute;n General</strong>
                    </div>
                    <div className="card-body">                    
                        <form>
                            <div className="form-group row">
                                <label htmlFor="inputNumero" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                        N&uacute;mero
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputNumero" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.numero}
                                        onChange={this._handleChangeNumero.bind(this)} 
                                        readOnly={this.state.nuevo}
                                    />
                                </div>
                                <label htmlFor="inputUsuario" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                        Usuario
                                </label>
                                <div className="col-md-7 col-lg-7">
                                    <input type="text" 
                                        className="form-control-plaintext form-control-xs" 
                                        readOnly
                                        id="inputUsuario" 
                                        value={this.props.authentication.user.name}
                                    />
                                </div>
                                
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputFecha" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Fecha
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputFecha" 
                                        type="text" 
                                        placeholder="dd/mm/yyyy"
                                        value={this.state.general.fecha}
                                        onChange={this._handleChangeFecha.bind(this)} 
                                    />
                                </div>
                                <label htmlFor="inputTipoComprobante" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                    Tipo Comprobante
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <div className="input-group input-group-xs">
                                        <input className="form-control py-1" 
                                            id="inputTipoComprobante" 
                                            type="text" 
                                            placeholder=""
                                            value={this.state.general.tipo_comprobante}
                                            onChange={this._handleChangeTipoComprobante.bind(this)}   
                                        />
                                        <div className="input-group-append">
                                            <ButtonSearchVoucherType 
                                                modalId="cnVoucherType"
                                                setValue={this._setVoucherType}/>
                                        </div>
                                    </div>   
                                </div>                         
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputGlosa" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Glosa
                                </label>
                                <div className="col-md-10 col-lg-11">
                                    <textarea className="form-control form-control-xs" 
                                        id="inputGlosa" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.glosa}
                                        onChange={this._handleChangeGlosa.bind(this)} 
                                    >
                                    </textarea>
                                </div>                        
                            </div> 
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <br />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <strong>Detalle</strong>&nbsp;
                        <button className="btn btn-outline-info btn-xs" type="button" onClick={this._handleClickAddRow.bind(this)}>
                            <i className="fas fa-plus"></i>&nbsp;Agregar
                        </button>     
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th scope="col" style={{width: '50px', textAlign: 'center'}}>L&iacute;nea</th>
                                    <th scope="col" colSpan="2">Cuenta</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Debe</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Haber</th>
                                    <th scope="col" style={{width: '150px', textAlign: 'center'}}>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.detalle.map((row, index) => {
                                        return(  
                                            <AccountDetail 
                                                key={index}
                                                index={index}
                                                line={row.line} 
                                                account_id={row.code}
                                                account_name={row.name}
                                                debit={row.debit}
                                                credit={row.credit}
                                                onChangeAccountId={this._handleChangeAccountDetailAccount}
                                                onChangeAccountIdInput={this._handleChangeAccountDetailAccountInput}
                                                onChangeDebit={this._handleChangeAccountDetailDebit}
                                                onChangeCredit={this._handleChangeAccountDetailCredit}
                                                onClickCopyRow={this._handleClickCopyRow.bind(this)}
                                                onClickDelRow={this._handleClickDelRow.bind(this)}
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>             
        </>
      );
    }
}
  
function AccountDetail(props){    

    function _setAccount(p_data){
        console.log("p_data",p_data)
        if(p_data.isLeaf){
            props.onChangeAccountId(props.index, p_data.id, p_data.account_code,p_data.name);
        }
    }

    function _handleChangeAccountId(e){
        console.log("props.index",props.index);
        props.onChangeAccountIdInput(props.index, e.target.value);
    }

    function _handleChangeDebit(e){
        props.onChangeDebit(props.index, e.target.value);
    }

    function _handleChangeCredit(e){
        props.onChangeCredit(props.index, e.target.value);
    }

    function _handleClickEditRow(e){
        //props.onClickDelRow(props.index);
    }

    function _handleClickCopyRow(e){
        props.onClickCopyRow(props.index);
    }

    function _handleClickDelRow(e){
        props.onClickDelRow(props.index);
    }

    return(
        <tr>
            <th scope="row">{props.line}</th>
            <td style={{width: '150px',}}>
                <div className="input-group input-group-xs">
                    <input className="form-control py-1" 
                        id="inputAccountId" 
                        type="text" 
                        placeholder=""
                        value={props.account_id}
                        onChange={_handleChangeAccountId}
                    />
                    <div className="input-group-append">
                        <ButtonSearchAccount 
                            modalId="cnAccount"
                            setValue={_setAccount}/>
                    </div>
                </div> 
            </td>
            <td>
                {props.account_name}
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id="inputDebit" 
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.debit}
                        onChange={_handleChangeDebit}
                />
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id="inputCredit" 
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.credit}
                        onChange={_handleChangeCredit}
                />
            </td>
            <td>
                <button className="btn btn-outline-primary btn-xs" type="button" onClick={_handleClickEditRow}>
                    <i className="fas fa-paperclip"></i>
                </button>&nbsp;
                <button className="btn btn-outline-info btn-xs" type="button" onClick={_handleClickCopyRow}>
                    <i className="fas fa-copy"></i>
                </button>&nbsp;
                <button className="btn btn-outline-danger btn-xs" type="button" onClick={_handleClickDelRow}>
                    <i className="fas fa-trash"></i>
                </button>
            </td>
        </tr>
    )
}