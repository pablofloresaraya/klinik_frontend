import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';

export function ButtonEliminar(props){
    
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleDelete = () => {
        props.delete();
        setShow(false);
    }

    return(
        <>
            <button className="btn btn-danger btn-sm btn-block" type="button" onClick={handleShow} disabled={props.disa} >Eliminar</button> 
            <Modal show={show} onHide={handleClose} size="sm" maria-labelledby="ModalHeader">
                <Modal.Header closeButton>
                    <Modal.Title id='ModalHeader'>Eliminar</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    ¿Está seguro de eliminar el registro?
                </Modal.Body>
                <Modal.Footer>
                    <button className='btn btn-primary' onClick={handleDelete} >
                        Aceptar
                    </button>                   
                    <button className='btn btn-secondary' onClick={handleClose}>
                        Cerrar
                    </button>
                </Modal.Footer>
            </Modal> 
        </>
    );
    
}

export default ButtonEliminar;