import React, {Component} from 'react';
import { Form } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import "./css/customDatePickerWidth.css";
import es from 'date-fns/locale/es';
registerLocale('es', es)

export class CgFrmLibroDiario extends Component{

    constructor(props){ 

        super(props);

        this.state = {
            radio: false            
            ,fechaini: ""
            ,fechafin: ""
            ,fechaperiodo:null
            ,disfecha:true
            ,disperiodo:true  
        }

        this.handleChangeRad = this.handleChangeRad.bind(this);
        this.handleChangeFechas = this.handleChangeFechas.bind(this);
        this.handleChangePeriodo = this.handleChangePeriodo.bind(this);

    }

    handleChangeRad(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state
            ,[name]:value                     
        });
        
        (this.state.radio==="F")
        ? this.setState({ fechaini: "", fechafin: "" })
        : this.setState({ fechaperiodo: null });
        

    }

    handleChangeFechas(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state.form,
            [name] : value                         
        })

    }

    handleChangePeriodo(fecha){
        this.setState({ fechaperiodo: fecha });
    }

    formatDate = (d) => {

        let month = d.getMonth();
        let day = d.getDate();
        month = month + 1;
        month = month + "";
        if (month.length === 1)
        {
            month = "0" + month;
        }
        day = day + "";
        if (day.length === 1)
        {
            day = "0" + day;
        }
        
        return day + '-' + month + '-' + d.getFullYear();
    
    };

    formatDateMonth = (d) => {

        let month = d.getMonth();

        month = month + 1;
        month = month + "";
        if (month.length === 1)
        {
            month = "0" + month;
        }

        return month + '-' + d.getFullYear();
    
    };

    render(){
        return(            
            <div className="row" >
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <strong>Par&aacute;metros del Informe</strong>
                        </div>
                        <div className="card-body">
                            <form name="libroForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                <div className="form-group row">
                                    <div className="col-md-1 col-lg-2">
                                        &nbsp;
                                    </div>
                                    <div className="col-md-1 col-lg-3">
                                        <div className="row">
                                            <div className="col-md-1 col-lg-6">
                                                <input name="radio" id="fecha" type="radio" value="F" checked={this.state.radio==="F"} onChange={this.handleChangeRad}  />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="fecha">
                                                    Por fecha
                                                </label>
                                            </div>
                                            <div className="col-md-1 col-lg-6">                                       
                                                <input name="radio" id="periodo" type="radio" value="P" checked={this.state.radio==="P"} onChange={this.handleChangeRad} />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="periodo">
                                                    Por per&iacute;odo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label control-label input-label text-right">Fecha Inicio</label>
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechaini" type="date" className="form-control" value={this.state.fechaini} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>    
                                    <label className="col-md-1 col-lg-2 col-form-label control-label input-label text-right">Fecha Termino</label>        
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechafin" type="date" className="form-control" value={this.state.fechafin} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>  
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label control-label input-label text-right">Per&iacute;odo</label>
                                    <div className="col-md-2 col-lg-3 customDatePickerWidth">
                                        <DatePicker
                                            name="fechaperiodo"
                                            locale={'es'}                                      
                                            selected={this.state.fechaperiodo}
                                            onChange={(date) => this.handleChangePeriodo(date)}
                                            dateFormat="MM-yyyy"
                                            showMonthYearPicker
                                            className="form-control"
                                            isClearable
                                            disabled={this.state.radio!=="P"}                                                                               
                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="row">
                            <div className="col-md-2 offset-md-3 offset-lg-4">
                                <button className="btn btn-primary btn-sm btn-block" type="button" onClick={this.handleSendData} >Generar Informe</button>  
                            </div>
                            <div className="col-md-2">
                                <button className="btn btn-secondary btn-sm btn-block" type="button" >Cerrar</button> 
                            </div>    
                        </div>&nbsp;    
                    </div>  
                </div>     
            </div>            
        )}

}