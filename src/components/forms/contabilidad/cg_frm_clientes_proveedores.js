import React, {Component} from 'react';
import { connect } from 'react-redux';
import { contaActions } from '../../_actions';
import { config } from '../../_constants';
import { authHeader } from '../../_helpers';
import {ButtonRutProv} from '../querys/modalrutprov/ButtonRutProv';
import {ButtonComuna} from '../querys/modalComuna/ButtonComuna';
import {ButtonCiudad} from '../querys/modalCiudad/ButtonCiudad';
import {ButtonEliminar} from './ButtonEliminar';
import Modal from 'react-bootstrap/Modal';
import {  validate, clean, format, getCheckDigit } from 'rut.js'

export class CgFrmClientesProveedores extends Component{

    constructor(props){ 

        super(props);

        this.state = {
            form: { 
                     id: ""
                    ,rut: ""
                    ,dig:""
                    ,cliente: false
                    ,proveedor: false
                    ,nombre: ""
                    ,giro: ""
                    ,direccion: ""
                    ,comuna: {id:"",name:""}
                    ,ciudad: {id:"",name:""}
                    ,telefono: ""
                    ,celular: ""
                    ,email: ""
                    ,sucursal: ""
                    ,email_suc: ""
                },
            validate: { 
                        email: { status:false, msj:"", clase:"" }
                        ,emailSuc: { status:false, msj:"", clase:"" }
                    },
            ruts:[],
            communes:[],
            cities:[],
            citiesxcommunes:[],
            btn_elimina:true,
            modal : {show:false, msg:"", clase:""}             
        } 

        this.handleChange       = this.handleChange.bind(this);
        this.handleSendData     = this.handleSendData.bind(this);
        this.handleDeleteData   = this.handleDeleteData.bind(this);
        this.handleChangeNum    = this.handleChangeNum.bind(this);
        this.handleChangeRut    = this.handleChangeRut.bind(this);
        this.handleClickCheck   = this.handleClickCheck.bind(this);       
        this.handleSetRut       = this.handleSetRut.bind(this);
        this.handleSetCity      = this.handleSetCity.bind(this);
        this.handleSetCommune   = this.handleSetCommune.bind(this);
        this.validateEmail      = this.validateEmail.bind(this);
        this.handleClose        = this.handleClose.bind(this);
        
    }

    getData(){

        //this.props.rutProv();

        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        
        return fetch(`${config.apiUrl}/accounts/cities`, requestOptions)
        .then(results => {
            return results.json();
        })
        .then(results => {
            this.setState(results.data);
            /*this.setState({
                        ruts:results.data.ruts
                        ,communes:results.data.communes
                        ,cities:results.data.cities
                    });*/            
        })


    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
    }

    handleChange(event){

        const { name, value } = event.target;

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }             
        })    
        //this.props.datosProv(name,value);
    }

    handleChangeNum(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value)
            }             
        })    
        
    }

    handleChangeRut(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value),
                dig: value ? getCheckDigit(removeNonNumeric(value)) : ""
            }             
        })    
        
    }

    handleClickCheck(event){

        const { name } = event.target;

        let check;

        check = (!this.state.form[name]) ? true : !this.state.form[name];
        
        this.setState({
          form: {
            ...this.state.form,
            [name] : check
          }           
        })       
               
    }

    validateEmail(e,tipo) {
        
        const { name, value } = e.target;        

        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
        const { validate } = this.state;

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }             
        })

        if(tipo==="EM"){ //email

            if (emailRex.test(value)) {
                validate.email.status = true;
                validate.email.clase  = "text-success";
                validate.email.msj    = "Email Correcto";
            } else {
                validate.email.status = true;
                validate.email.clase  = "text-danger";
                validate.email.msj    = "Email Incorrecto";
            }

            if(value===""){
                validate.email.status = false;
                validate.email.clase  = "";
                validate.email.msj    = "";
            }

        }else{ //email sucursal

            if (emailRex.test(value)) {
                validate.emailSuc.status = true;
                validate.emailSuc.clase  = "text-success";
                validate.emailSuc.msj    = "Email Correcto";
            } else {
                validate.emailSuc.status = true;
                validate.emailSuc.clase  = "text-danger";
                validate.emailSuc.msj    = "Email Incorrecto";
            }

            if(value===""){
                validate.emailSuc.status = false;
                validate.emailSuc.clase  = "";
                validate.emailSuc.msj    = "";
            }

        }

        this.setState({ validate });

    }

    handleSetRut(p_rut) {

        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        
        return fetch(`${config.apiUrl}/accounts/${p_rut}`, requestOptions)
        .then(results => {
            return results.json();
        })
        .then(results => {
            if(results.data){
                this.setState({form:results.data});
                this.setState({btn_elimina:false});
            }           
            //console.log(JSON.stringify(results.data));
        })
              
    }

    handleSetCommune(p_id,p_name) {

        //limpio ciudad
        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                ciudad: {                     
                    ...prevState.form.ciudad,   
                    id : "",
                    name : ""          
                }
            }
        }))

        //add comuna
        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                comuna: {                     
                    ...prevState.form.comuna,   
                    id : p_id,
                    name : p_name          
                }
            }
        }))

        //filtra ciudades x comuna
        const city = this.state.cities.filter(cities => cities.parent_id === p_id);
        this.setState({citiesxcommunes:city});

    }

    handleSetCity(p_id,p_name) {

        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                ciudad: {                     
                    ...prevState.form.ciudad,   
                    id : p_id,
                    name : p_name          
                }
            }
        }))

    }
    
    validateEmailSend = (email) => {
        const re =
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleClose(){
        this.setState({
            modal: {
                ...this.state.modal,
                    show  : false                    
            }             
        })
    }

    handleSendData() {
        //console.log(JSON.stringify(this.state.form));
        if( !this.validateEmailSend(this.state.form.email) || !this.validateEmailSend(this.state.form.email_suc) ){
            this.setState({
                modal: {
                    ...this.state.modal,
                        show  : true,
                        msg   : "Existe email incorrecto.",
                        clase : ""        
                    
                }             
            })
            return false; 
        }

        const requestOptions = {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(this.state.form)
        };
        
        return fetch(`${config.apiUrl}/accounts/proveedor`,requestOptions)
        .then(results => {
            return results.json();
        })
        .then(results => {
            console.log(JSON.stringify(results.data));
            if(results.data.check){
                this.setState({form:results.data.clean,modal:results.data.modal,validate:results.data.validate});   
                this.getData();
            }else{
                this.setState({modal:results.data.modal}); 
            }
        });

    }

    handleDeleteData() {

        const requestOptions = {
            method: 'PUT',
            headers: authHeader(),
            body: JSON.stringify({"customer":this.state.form.cliente,"supplier":this.state.form.proveedor})
        };
        
        return fetch(`${config.apiUrl}/accounts/delete/${this.state.form.id}`,requestOptions)
        .then(results => {
            return results.json();
        })
        .then(results => {
            console.log(JSON.stringify(results.data));
            if(results.data.check){
                this.setState({form:results.data.clean});
                this.getData();
                this.setState({btn_elimina:true});
            }else{
                this.setState({modal:results.data.modal});
            }
        })

    }

    //value={(this.state.form.rut!=="") ? getCheckDigit(this.state.form.rut) : ""}
    
    render(){

        return( 
        <>           
            <div className="row" >
                <div className="col-lg-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <strong>Informaci&oacute;n del Proveedor</strong>
                        </div>
                        <div className="card-body"> 
                            <form name="clieProvForm" className="form-horizontal" style={{marginTop:"1rem"}}>                      
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Rut</label>
                                    <div className="col-md-2">                                     
                                        <input name="rut" type="text" className="form-control text-center" maxLength="8" value={this.state.form.rut} onChange={this.handleChangeRut} placeholder="RUT" />
                                    </div> 
                                    <div className="input-group col-md-1">                                                                                                                   
                                        <input name="dig" type="text" className="form-control text-center" style={{maxWidth:"40px"}} value={this.state.form.dig} disabled />                                       
                                        <div className="input-group-append">
                                            <ButtonRutProv data={this.state.ruts} rut={this.handleSetRut} />
                                        </div>                                              
                                    </div>
                                    <div className="col-md-1 form-check form-check-inline">
                                        <input className="form-check-input" type="checkbox" name="cliente" id="cliente" value={this.state.form.cliente} checked={this.state.form.cliente} onChange={this.handleClickCheck} />
                                        <label className="form-check-label col-form-label control-label input-label" htmlFor="cliente">
                                            Cliente
                                        </label>
                                    </div>                            
                                    <div className="col-md-1 form-check form-check-inline">
                                        <input className="form-check-input" type="checkbox" name="proveedor" id="proveedor" value={this.state.form.proveedor} checked={this.state.form.proveedor} onChange={this.handleClickCheck} />
                                        <label className="form-check-label col-form-label control-label input-label" htmlFor="proveedor">
                                            Proveedor
                                        </label>
                                    </div>                            
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Nombre</label>
                                    <div className="col-md-5">
                                        <input name="nombre" type="text" className="form-control input-sm text-center" value={this.state.form.nombre} onChange={this.handleChange} placeholder="Nombre" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Giro</label>
                                    <div className="col-md-5">
                                        <input name="giro" type="text" className="form-control input-sm text-center" value={this.state.form.giro} onChange={this.handleChange} placeholder="Giro" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Direcci&oacute;n</label>
                                    <div className="col-md-5">
                                        <input name="direccion" type="text" className="form-control input-sm text-center" value={this.state.form.direccion} onChange={this.handleChange} placeholder="Direccion" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Comuna</label>
                                    <div className="input-group col-md-5">
                                        <input type="text" className="form-control" value={this.state.form.comuna.name} placeholder="Comuna" aria-label="comuna" aria-describedby="button-comuna" disabled />
                                        <div className="input-group-append">
                                            <ButtonComuna data={this.state.communes} comuna={this.handleSetCommune} />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Ciudad</label>
                                    <div className="input-group col-md-5">
                                        <input type="text" className="form-control" value={this.state.form.ciudad.name} placeholder="Ciudad" aria-label="ciudad" aria-describedby="button-ciudad" disabled />
                                        <div className="input-group-append">
                                            <ButtonCiudad data={this.state.citiesxcommunes} ciudad={this.handleSetCity} />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Telefono</label>
                                    <div className="col-md-2">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text" id="btnGroupAddon">+57</div>
                                            </div>
                                            <input name="telefono" type="text" className="form-control input-sm text-center" maxLength="7" value={this.state.form.telefono} onChange={this.handleChangeNum} placeholder="Telefono" />
                                        </div>
                                    </div>
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Celular</label>
                                    <div className="col-md-2">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text" id="btnGroupAddon">+56</div>
                                            </div>
                                            <input name="celular" type="text" className="form-control input-sm text-center" maxLength="9" value={this.state.form.celular} onChange={this.handleChangeNum} placeholder="Celular" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Email</label>
                                    <div className="col-md-5">
                                        <input name="email" type="text" className="form-control input-sm text-center" value={this.state.form.email} onChange={(e) => this.validateEmail(e,"EM")} placeholder="Email" />
                                    </div>
                                    {this.state.validate.email.status && <label className={`col-md-2 col-lg-2 col-form-label control-label input-label text-left ${this.state.validate.email.clase}`}>{ this.state.validate.email.msj }</label> }                                 
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Sucursal</label>                                
                                    <div className="col-md-5">
                                        <input name="sucursal" type="text" className="form-control input-sm text-center" value={this.state.form.sucursal} onChange={this.handleChange} placeholder="Sucursal" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label control-label input-label text-right">Email Suc.</label>
                                    <div className="col-md-5">
                                        <input name="email_suc" type="text" className="form-control input-sm text-center" value={this.state.form.email_suc} onChange={(e) => this.validateEmail(e,"ES")} placeholder="Email Sucursal" />
                                    </div>
                                    {this.state.validate.emailSuc.status && <label className={`col-md-2 col-lg-2 col-form-label control-label input-label text-left ${this.state.validate.emailSuc.clase}`}>{ this.state.validate.emailSuc.msj }</label> } 
                                </div>                      
                            </form>
                        </div>
                        <div className="row">
                            <div className="col-sm-1 offset-sm-2">
                                <button className="btn btn-primary btn-sm btn-block" type="button" onClick={this.handleSendData} >Guardar</button>  
                            </div>
                            <div className="col-sm-1">
                                <ButtonEliminar delete={this.handleDeleteData} disa={this.state.btn_elimina} /> 
                            </div>
                            <div className="col-sm-1">
                                <button className="btn btn-secondary btn-sm btn-block" type="button" >Cerrar</button> 
                            </div>    
                        </div>&nbsp; 
                    </div>   
                </div>
            </div>
            <Modal show={this.state.modal.show} onHide={this.handleClose} size="sm" maria-labelledby="ModalHeader">
                <Modal.Header closeButton>
                    <Modal.Title id='ModalHeader'>Alerta</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.modal.msg}
                </Modal.Body>
                <Modal.Footer>                   
                    <button className='btn btn-primary' onClick={this.handleClose}>
                        Cerrar
                    </button>
                </Modal.Footer>
            </Modal> 
        </>          
        )
    }

}

/*function mapStateToProps(state) {
    
    const rutprov = state.contabilidad;    
    return rutprov;
}

const mapDispatchToProps = {
    rutProv: contaActions.rutProv,
    datosProv: contaActions.datosProv
};

const connectProv = connect(mapStateToProps, mapDispatchToProps)(CgFrmClientesProveedores);
export { connectProv as CgFrmClientesProveedores };*/