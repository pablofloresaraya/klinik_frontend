import { menuConstants } from '../_constants';

const initialState = { 
  menuModuleActive: 0, 
  menuNameModuleActive: "",
  moduleApplications: [],
  moduleSections: [] ,
  moduleUiMenu: []
};

export function menu(state = initialState, action) {
  switch (action.type) {
    case menuConstants.SHOW_MODULE:
      return {
        menuModuleActive: action.moduleId,
        menuNameModuleActive: action.moduleName,
        moduleSections: [],
        moduleApplications: [],
        moduleUiMenu: []
      };
    case menuConstants.SHOW_SECTIONS_SUCCESS:
      return {
        menuModuleActive: state.menuModuleActive,
        menuNameModuleActive: state.menuNameModuleActive,
        moduleSections: action.sections,
        moduleApplications: [],
        moduleUiMenu: []
      };
    case menuConstants.SHOW_MODULE_FAILURE:
      return {
        menuModuleActive: state.menuModuleActive,
        menuNameModuleActive: state.menuNameModuleActive,
        moduleSections: [],
        moduleApplications: [],
        moduleUiMenu: []
      };
    case menuConstants.SET_UI_MENU_SUCCESS:
      return {
        menuModuleActive: state.menuModuleActive,
        menuNameModuleActive: state.menuNameModuleActive,
        moduleUiMenu: action.uimenu,
        moduleSections: action.sections,
        moduleApplications: [],
      };
    default:
      return state
  }
}