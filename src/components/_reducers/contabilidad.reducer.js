import { contaConstants } from '../_constants';

const initialState = {
    form: {
        rut:'',
        dig:'',
        cliente:'',
        proveedor:'',
        nombre:'',
        giro:'',
        direccion:'',
        comuna:'',
        ciudad:'',
        telefono:'',
        celular:'',
        email:'',
        sucursal:'',
        emailsuc:''
    },
    rutprov : []
    
};

export function contabilidad (state = initialState, action) {

    //const { type, field, payload } = action;
    console.log('-> '+action.field);
    switch (action.type) {
        case contaConstants.WRITE:          
        return {
            ...state,
            form:{ 
                [action.field]: action.value 
            }
            }; 
        case contaConstants.SUCCESS:          
            return {
                ...state, 
                rutprov: action.info 
                };          
        case contaConstants.CLEAR:
          return initialState;
        default:
          return state;
    }
  
}
  
//export default contabilidad;