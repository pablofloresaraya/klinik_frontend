import { contaConstants } from '../_constants';
import { contaService } from '../_services';
import { alertActions } from './';

export const contaActions = {
    rutProv,
    datosProv
};

function rutProv() {
    //console.log('-> rutProv');
    return dispatch => { 
        //dispatch(request());

        contaService.getProv()
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    //function request() { return { type: contaConstants.GETRUTS } }
    function success(data) { return { type: contaConstants.SUCCESS, info:data } }
    function failure(error) { return { type: contaConstants.FAILURE, error } }
}

function datosProv(p_name,p_value) {
    //console.log('-> rutProv');
    return dispatch => { 
        dispatch({type: contaConstants.WRITE, field:p_name, value:p_value});
    };

    //function success(p_name,p_value) { return { type: contaConstants.WRITE, field:p_name, values:p_value } }
    
}