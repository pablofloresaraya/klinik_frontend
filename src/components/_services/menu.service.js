import { config } from '../_constants';
import { authHeader } from '../_helpers';

export const menuService = {
    getModuleSections,
    getModuleApplications,
    getModuleUiMenu
};

function getModuleUiMenu(p_module_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/${p_module_id}/all`, requestOptions)
        .then(handleResponse)
        .then(menus => { 
            return menus.data;
        });
}

function getModuleSections(p_module_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/${p_module_id}/sections`, requestOptions)
        .then(handleResponse)
        .then(sections => {
            return sections.data;
        });
}

function getModuleApplications(p_module_id, p_section_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/applications?module_id=${p_module_id}&section_id=${p_section_id}`, requestOptions)
        .then(handleResponse)
        .then(applications => {
            return applications;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                console.log("No fue posible obtener las aplicaciones existentes en el modulo");
            }

            const error = (data && data.message) || response.statusText;
            console.log("error:", error);

            return Promise.reject(error);
        }

        return data;
    });
}