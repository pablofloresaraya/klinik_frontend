import { config } from '../_constants';
import { authHeader } from '../_helpers';

export const contaService = {
    getProv
};

function getProv() {
    
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    
    return fetch(`${config.apiUrl}/cities`, requestOptions)
    .then(results => {
        return results.json();
    })
    .then(results => {
        //console.log(JSON.stringify(results));
        return results.data;               
    })
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                //logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}    