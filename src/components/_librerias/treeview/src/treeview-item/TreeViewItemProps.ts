import TreeView from '../treeview/TreeView';

export interface TreeviewItemProps {
  treeview: TreeView,
  item: number,
  parent?: any,
  level: number,
  root?: any,
  selectRow?: boolean,
  expandAll?: boolean
}