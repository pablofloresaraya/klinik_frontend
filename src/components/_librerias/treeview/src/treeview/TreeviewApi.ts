import TreeviewApiImpl from './TreeviewApiImpl'
import { load, loadData, setTheme } from './TreeviewHelper'

const treeviewApi = scope => {
  return {

    //
    //
    //
    addItem: (text, isLeaf, parentNode) => {
      return TreeviewApiImpl.addItem(scope, text, isLeaf, parentNode);
    },

    //
    // folderToFind can be passed as a "id" or as a "object" ex:
    //
    //  treeviewEl.api.findFolder(456) //456 is a id value or
    //  treeviewEl.api.findFolder({name: 'Brazil'}) //it will searches for the first folder that match the passed data and leaf is not true
    //
    findFolder: (folderToFind) => {
      return TreeviewApiImpl.findFolder(scope, folderToFind);
    },

    //
    // Expand all the children from the rootItem recursively
    expandAll: () => {
      TreeviewApiImpl.expandAll(scope);
    },
    //
    // folderToFind can be passed as a "id" or as a "object" ex:
    //
    //  treeviewEl.api.findFolder(456) //456 is a id value or
    //  treeviewEl.api.findFolder({name: 'Brazil'}) //it will searches for the first folder that match the passed data and leaf is not true
    //
    findNode: (nodeToFind) => {
      return TreeviewApiImpl.findNode(scope, nodeToFind);
    },

    //
    // itemToFind can be passed as a "id" or as a "object" ex:
    //
    //  treeviewEl.api.findItem(357) //357 is a id value or
    //  treeviewEl.api.findItem({name: 'Dog'}) //it will searches for the first item that match the passed data and leaf is true
    //
    findItem: (itemToFind) => {
      return TreeviewApiImpl.findItem(scope, itemToFind);
    },

    //
    //
    //
    getItems: () => {
      return TreeviewApiImpl.getItems(scope);
    },

    //
    //
    //
    getParentNode: (item) => {
      return TreeviewApiImpl.getParentNode(scope, item);
    },


    //
    //
    //
    getRootItem: () => {
      return TreeviewApiImpl.getRootItem(scope);
    },

    //
    //
    //
    getSelectedItem: () => {
      return TreeviewApiImpl.getSelectedItem(scope);
    },

    //
    //
    //
    load: item => load(scope, item),  

    //
    //
    //
    loadData: loadData.bind(scope),

    //
    //
    //
    removeItem: (id) => {
      TreeviewApiImpl.removeItem(scope, id);
    },

    //
    // itemToFind can be passed as a "id" or as a "object" ex:
    //
    //  treeviewEl.api.selectItem(357) //357 is a id value or
    //  treeviewEl.api.selectItem({name: 'Dog'}) //it will searches for the first item that match the passed data and leaf is true
    //
    selectItem: (itemToFind) => {
      TreeviewApiImpl.selectItem(scope, itemToFind);
    },

    //
    //
    //
    setTheme: (newTheme) => {
      setTheme(scope, newTheme);
    },

  }

}

export {
  treeviewApi
};