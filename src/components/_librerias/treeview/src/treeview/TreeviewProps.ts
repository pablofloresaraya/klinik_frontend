import { CSSProperties } from "react";
import TreeView from "./TreeView";

export interface TreeviewProps {
    className?: string,
    style?: CSSProperties,
    actionButtons?: JSX.Element[], 
    autoLoad?: boolean,
    json?: string,
    items?: any[],
    lazyLoad?: boolean,
    expandAll?: boolean,
    marginItems?: string | number,
    onAfterLoad?: void,
    onBeforeLoad?: (data: any, item: any) => void;
    onRenderItem?: (item: any, treeview: TreeView) => JSX.Element;
    onSelectItem?: void,
    onCheckItem?: void,
    selectRow?: boolean,
    showCheckbox?: boolean,
    showIcon?: boolean,
    showRoot?: boolean,
    theme?: string,
    url?: string,

    onActionButtonClick?: (item: any, actionButton: any) => void;
}