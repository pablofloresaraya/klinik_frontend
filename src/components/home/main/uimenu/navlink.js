import React from 'react';

export function NavLink(props){
  return(
    <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {props.name}
    </a>
  )
}