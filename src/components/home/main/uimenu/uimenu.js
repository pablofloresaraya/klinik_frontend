import React, {Component} from 'react';
import {ViewUIMenu} from "./viewuimenu";
import {NavbarNav} from "./navbarnav";

import $ from 'jquery';

export class UIMenu extends Component{
    constructor(props) {
        super(props);
        this._handleOnClickApplication = this._handleOnClickApplication.bind(this);
    }

    componentDidMount(){

        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });
    }

    _handleOnClickApplication(app){
        this.props.addApplicationTab(app);
    }

    render(){
        console.log("4. UIMenu props", this.props);
        return(
            <nav  className="navbar navbar-expand-lg navbar-light bg-light"
                style={{ paddingTop: '1px', paddingBottom: '1px', paddingRight: '10rem'}}
            >
                <a className="navbar-brand" 
                    href="/#"
                    style={{ color: 'rgba(162, 157, 157, 0.5)', fontSize: '1.5rem', padding: '0px'}}
                >
                    <strong>{this.props.menuNameModuleActive}</strong>
                </a>
                <button
                        className="navbar-toggler navbar-toggler-right"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <NavbarNav>
                        {this.props.moduleUiMenu.length>0 
                            && 
                        <ViewUIMenu 
                            childrens={this.props.moduleSections} 
                            uimenu={this.props.moduleUiMenu} 
                            moduleId={this.props.menuModuleActive} 
                            iteracion={0} 
                            _handleOnClickApplication={this._handleOnClickApplication}
                        />
                        }
                    </NavbarNav>
                </div>
            </nav>
        );
    }
}