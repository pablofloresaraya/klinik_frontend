import React, {Component} from 'react';

import ModuleSection from './modulesection'

export class ModuleMenu extends Component{
    constructor(props) {
        super(props);

        this.showModuleSection = this.showModuleSection.bind(this);

    }

    showModuleSection(){
      var sections = this.props.moduleSections;
      if(sections instanceof Array){
        if(sections.length>0){
          const secciones = sections.map((section, index) =>
              <ModuleSection 
                    key={index} 
                    id={section.md_cod} 
                    name={section.md_nombre} 
                    moduleId={this.props.moduleActive}
                    addApplicationTab={this.props.addApplicationTab}
                    applicationActive={this.props.applicationActive}
              />
          );

          return (secciones)
        }
      }
    }

    render(){
      console.log("4. ModuleMenu props", this.props);

      return(
        <nav  className="navbar navbar-expand-lg navbar-light bg-light" 
              style={{ paddingTop: '1px', paddingBottom: '1px'}}
        >
          <a className="navbar-brand" href="#" 
              style={{ color: 'rgba(162, 157, 157, 0.5)', fontSize: '1.5rem', padding: '0px'}} 
          >
              <strong>{this.props.moduleName}</strong>
          </a>
          <button 
                  className="navbar-toggler navbar-toggler-right" 
                  type="button" 
                  data-toggle="collapse" 
                  data-target="#navbarSupportedContent" 
                  aria-controls="navbarSupportedContent" 
                  aria-expanded="false" 
                  aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">   
              {this.showModuleSection()}
            </ul>
          </div>    
      </nav>
      );
    }
  }